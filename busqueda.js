var lista = [1,2,3,4,5,6,7,8,9,10,11,23,45,66];

$(document).ready(function () {
    $("#lista").val(lista);
    $('#btn-buscar').click(function (e) {
        var dateStart = new Date();
        var valor = $("#valor").val();
        if(valor == null || valor == ''){
            $('#encontrado').val('Ingresar número');
            $('#encontrado').attr('hidden', false);
            return;
        }else{
            var posicion = busquedaB(valor,lista);
            if(posicion > -1){
                $('#encontrado').val('La Posición de '+ valor +' es: ' + posicion);
                $('#encontrado').attr('hidden', false); 
            }else{
                $('#encontrado').val('El número '+valor+ ' no ha sido encontrado');
                $('#encontrado').attr('hidden', false);
            }    
        }
    });
});

function busquedaB(valor, lista) {
    let min = 0;   
    let max = lista.length - 1;  
    let posicion = -1;
    let encontrado = false;
    let dato;
 
    while (encontrado === false && min <= max) {
        dato = Math.floor((min + max)/2);
        if (lista[dato] == valor) { //Lo encontraste
            encontrado = true;
            posicion = dato;
        } else if (lista[dato] > valor) { //intento demasiado alto
            max = dato - 1;
        } else { //intento demasiado bajo
            min = dato + 1;
        }
    }
    return posicion;
}